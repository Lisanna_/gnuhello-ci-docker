# gnuhello-ci-docker

Demonstration of declarative docker-based pipelines in GitLab using GNU Hello as
an example.

Interesting files:
- [`.gitlab-ci.yml`](./.gitlab-ci.yml)
- [`docker/ubuntu1804/Dockerfile`](./docker/ubuntu1804/Dockerfile)
- [`ci-build-linux.sh`](./ci-build-linux.sh)

Check out this [successful pipeline][pipe] demonstrating the `ubuntu1804` CI job
running successfully.

[pipe]: https://gitlab.com/Lisanna_/gnuhello-ci-docker/pipelines/75528092
